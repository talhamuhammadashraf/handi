/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import firebase from 'firebase'
// import 'core-js/es6/symbol'
// import 'core-js/fn/symbol/iterator'
// var config = {
//     apiKey: "AIzaSyBEyvq6orAN3V_9tswPaj9O4CAwBHxRWLg",
//     authDomain: "blazing-heat-7297.firebaseapp.com",
//     databaseURL: "https://blazing-heat-7297.firebaseio.com",
//     projectId: "blazing-heat-7297",
//     storageBucket: "blazing-heat-7297.appspot.com",
//     messagingSenderId: "912168317097"
//   };
//   firebase.initializeApp(config);

AppRegistry.registerComponent(appName, () => App);
