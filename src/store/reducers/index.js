import {combineReducers} from 'redux'
import navReducer from './navReducer'
import Auth from './auth'

const appReducer = combineReducers({
  nav: navReducer
  // Auth
});

export default appReducer