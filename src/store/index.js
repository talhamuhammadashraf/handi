import {
    createStore,
    applyMiddleware,
  } from 'redux';
  import thunk from 'redux-thunk'
  import logger from 'redux-logger'
  // import createSagaMiddleware from 'red  ux-saga'
  // import rootSaga from './sagas'
  
  import {
    createReactNavigationReduxMiddleware,
  } from 'react-navigation-redux-helpers';
  import appReducer from './reducers'
  
  // const sagaMiddleware = createSagaMiddleware()
  const middleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
  );



const store = createStore(
    appReducer,
    applyMiddleware(middleware,thunk,logger),
  );
// sagaMiddleware.run(rootSaga)
  
  export default store

// import {
//   applyMiddleware,
//   compose,
//   createStore
// } from 'redux'
// import createSagaMiddleware from 'redux-saga'

// import reducer from './reducer'

// const enhancers = compose(
//   applyMiddleware(sagaMiddleware),
//   window.devToolsExtension ? window.devToolsExtension() : f => f
// )

// const store = createStore(
//   reducer,
//   enhancers
// )


// export default store