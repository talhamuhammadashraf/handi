import React ,{Component} from 'react';
import {View,Image,Text,TouchableOpacity,Dimensions,ScrollView} from 'react-native'
import Header from '../assets/header.png';
import {Input,Button} from '../components'
import Icon from 'react-native-vector-icons/FontAwesome'
import {} from "react-navigation"
// import {connect} from 'react-redux'
// import {bindActionCreators} from 'redux'
// import {Authentication} from '../store/actions/auth'
const {height,width} = Dimensions.get('window');

// const mapStateToProps = (state) =>({
//     // state:state
// })
// const mapDispatchToProps = (dispatch) =>bindActionCreators({
//     // signup:(user)=>Authentication.registerUser(user)
// },dispatch)
class SignUp extends Component{
    constructor(){
        super();
        this.state={
            height:height,
            width:width
        }
    }
    componentDidMount(){
    }
    render(){console.log(this.state)
        const {width,height} =this.state    
        return(
            <ScrollView contentContainerStyle={{
                backgroundColor:'#3B84A5'
            }}>
                <Image source={Header} style={{width:width,height:height*0.2}} />
            <View
            style={{
                flex:1,
                flexDirection:"row",
                justifyContent:"space-between",
                width:"80%",
                marginLeft:"auto",
                marginRight:"auto",
                borderBottomWidth:1,
                borderBottomColor:"black",
                marginBottom:10
            }}
            >
            <TouchableOpacity>
            <Icon size={35} name="angle-double-left" style={{marginLeft:5}} />                
            </TouchableOpacity>
            <Text style={{
                        // fontWeight:"700",
                        color:"white",
                        fontSize:18,
                        textAlign:"center",
                        margin:8,
                        textShadowColor: '#000000',
                        textShadowOffset: { width: 1, height: 2.5 },
                        textShadowRadius: 8
                    }}>
            Register for Handi
            </Text>
            <Text></Text>
            </View>
                <Input placeholder="Mobile Number*" icon="smartphone" keyboardType='Numeric' onChange={(ev)=>this.setState({mobile:ev.nativeEvent.text})}/>
                <Input placeholder="Email Address*" icon="mail" onChange={(ev)=>this.setState({email:ev.nativeEvent.text})}/>
                <Input placeholder="Name*" icon="user" onChange={(ev)=>this.setState({name:ev.nativeEvent.text})}/>
                <Input placeholder="Password*" icon="lock" onChange={(ev)=>this.setState({password:ev.nativeEvent.text})} secureTextEntry/>
                {/* <Input placeholder="Confrim Password*" icon="check" onChange={(ev)=>this.setState({confim:ev.nativeEvent.text})} secureTextEntry/> */}
                <Button 
                style={{marginTop:20}}
                title="Sign Up"
                />
            <View>
                <Text
                style={{color:"white",textAlign:"center",fontWeight:"bold"}}
                >
                    OR {"\n"}
                    Already have an account?
                </Text>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("LogIn")}>
                <Text style={{
                        fontWeight:"700",
                        color:"white",
                        fontSize:24,
                        textAlign:"center",
                        margin:8,
                        textShadowColor: '#000000',
                        textShadowOffset: { width: 1, height: 2.5 },
                        textShadowRadius: 8
                    }}>
                    Log In
                </Text>
                </TouchableOpacity>
                <Text></Text>
            </View>
            </ScrollView>
        )
    }
}
export default SignUp
// export default connect(mapStateToProps,mapDispatchToProps)(SignUp)