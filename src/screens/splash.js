import React ,{Component} from 'react';
import {View,Image,Dimensions,ScrollView} from 'react-native'
import splash from '../assets/splash.jpg';
import {} from "react-navigation"
const {height,width} = Dimensions.get('window')
class Splash extends Component{
    constructor(){
        super();
        this.state={
            height:height > width ? height : width,
            width:width
        }
    }
    componentDidMount(){
        setTimeout(()=>{this.props.navigation.navigate("SignUp")},1000)
    }
    render(){
        const {width,height} =this.state    
        return(
            <ScrollView onLayout={(event)=>{
                this.setState({
                    width: event.nativeEvent.layout.width,
            })}}
            >
                <Image source={splash} style={{width:width,height:height}} />
            </ScrollView>
        )
    }
}
export default Splash