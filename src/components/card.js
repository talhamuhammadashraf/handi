import React ,{Component} from 'react';
import {View,Text,Image,TouchableOpacity,Dimensions} from 'react-native';
const {height,width} = Dimensions.get('window')

const Card = (props) => {
    console.log(width,"this is width")
    return(
        <View 
        style={{
            width:props.full ? width*0.9 : width*0.45,
            marginHorizontal: props.full ? width*0.05 : width*0.025,
            height:height*0.35,
            marginVertical:width*0.025,
            backgroundColor:'white',
            justifyContent:"center",
            alignItems:"center",
            borderRadius:10
        }}
        >
        <Image
        source={props.image}
        style={{
            width:props.full ? width*0.9 : width*0.45,
            height:height*0.25,
            borderTopLeftRadius:10,
            borderTopRightRadius:10
        }}
        />
        <View style={{
            height:height*0.1,
            alignItems:'center',
            flexDirection:"row"
            }}>
        <Text 
        style={{
            color:'#3B84A5',
            fontSize:16,
            fontStyle:'italic'
            // alignItems:'center'
        }}
        >{props.title}</Text>
        </View>
        </View>
    )
}
export default Card