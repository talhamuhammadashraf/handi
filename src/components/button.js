import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Button = (props) => {
    return (
        <TouchableOpacity
        style={{
            width:"75%",
            marginRight:"auto",
            marginLeft:"auto",
        }}
        >
            <View
            style={[{
                // borderRadius:props.bordered ? 5 : 2,
                // borderColor:"#CECDCB",
                // borderWidth:props.bordered ? 2 : 1,
                
            },props.style]}
            >
                <LinearGradient colors={['#E6E7E9', '#156F87']} style={{
                    borderRadius:props.bordered ? 5 : 2,
                    borderColor:"#CECDCB",
                    borderWidth:props.bordered ? 2 : 1,                    
                }}>
                    <Text style={{
                        fontWeight:"700",
                        color:props.bordered ? "#676668" : "white",
                        fontSize:24,
                        textAlign:"center",
                        margin:8,
                        textShadowColor: '#000000',
                        textShadowOffset: props.bordered ? { width: 0, height: 0 } : { width: 1, height: 2.5 },
                        textShadowRadius: props.bordered ? 0 : 8
                    }}>
                        {props.title}
                    </Text>
                </LinearGradient>
            </View>
        </TouchableOpacity>
    )
}
export default Button