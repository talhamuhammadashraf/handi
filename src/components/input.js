import React, { Component } from "react";
import {View,Text,Image,TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
//check ,mail,smartphone,lock,user
const Input = (props) => {
    return(
    <View
    style={{
        borderColor:props.bordered ? "#D1D1D1" : "#C7CCD2",
        borderWidth:props.bordered ? 2 :1,
        borderRadius:props.bordered ? 5 : 0.5,
        width:"75%",
        marginRight:"auto",
        marginLeft:"auto",
        flex:1,
        flexDirection:"row",
        alignItems:"center",
        marginTop:4,
        marginBottom:4

    }}
    >
    <Icon size={35} name={props.icon} style={{marginLeft:5}} />
        <TextInput 
        placeholderTextColor= {props.bordered ? "#6E6A69" : "#9FBACB"} 
        style={{
            textAlign:props.bordered ? "left" : "center",
            fontSize:16,
            color:props.bordered ? "#6E6A69" : "#C7CCD2",
            width:"85%"
        }}
        {...props}
        />
    </View>
)}
export default Input